import { Link, routes, usePageLoadingContext } from '@redwoodjs/router'

const BlogLayout = ({ children }) => {
  const { loading } = usePageLoadingContext()

  return (
    <div className="max-w-8xl mx-auto">
      {loading && (
        <div className="fixed inset-0 z-50 overflow-auto bg-black opacity-25 flex">
          <div className="w-32 h-32 bg-white p-8 rounded-md relative m-auto">
            Loading ...
          </div>
        </div>
      )}
      <div className="sm:mx-8">
        <header className="md:flex flex-wrap items-center border-b-4 border-indigo-300 bg-indigo-600 text-white px-8 py-6 ">
          <nav className="flex-grow sm:flex-grow-0 min-h-screenmt-4 mt-4 md:mt-0">
            <ul className="flex justify-center">
              <li className="mx-4 font-semibold uppercase">
                <Link
                  to={routes.home()}
                  className="text-indigo-200 hover:text-indigo-800"
                >
                  Home
                </Link>
              </li>
              <li className="mx-4 font-semibold uppercase">
                <Link
                  to={routes.about()}
                  className="text-indigo-200 hover:text-indigo-800"
                >
                  About
                </Link>
              </li>
            </ul>
          </nav>
        </header>
        <main className="flex items-start">
          <section className="flex-1 pt-4 pb-8 px-8 bg-white shadow">
            {children}
          </section>
        </main>
        <footer className="bg-indigo-600 text-indigo-200 text-sm text-center py-4">
          Copyright ©{new Date().getFullYear()} Home, Inc.
        </footer>
      </div>
    </div>
  )
}

export default BlogLayout
