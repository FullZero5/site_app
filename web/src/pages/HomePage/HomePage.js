import BlogLayout from 'src/layouts/BlogLayout/BlogLayout'
import BlogPostsCell from 'src/components/BlogPostsCell'

const HomePage = () => {
  return (
    <BlogLayout>
      <section className="py-12">
        <div className="container mx-auto">
          <div>
            <h1 className="text-2xl font-black text-gray-900 pb-6 px-6 md:px-12">
              HomePage
            </h1>
          </div>
          <div className="grid grid-cols-3 gap-4">
            <BlogPostsCell />
          </div>
        </div>
      </section>
    </BlogLayout>
  )
}

export default HomePage
