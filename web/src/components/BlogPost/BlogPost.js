import { Link, routes } from '@redwoodjs/router'
const convertDate = (date) => new Date(date).toLocaleDateString('ru-RU')

const BlogPost = ({ post }) => {
  return (
    <article key={post.id} className="w-full lg:w-1/2   md:px-4 lg:px-6 py-5">
      <div className="bg-white hover:shadow-xl">
        <header className="px-4 py-4 md:px-10">
          <h2 className="font-bold text-lg">
            <Link to={routes.blogPost({ id: post.id })}>{post.title}</Link>
          </h2>
        </header>
        <p className="py-4">{post.body}</p>
        <div className="flex flex-wrap pt-8">
          <p className="w-full md:w-1/3 text-sm font-medium">Posted at:</p>
          <time className="2/3">{convertDate(post.createdAt)}</time>
        </div>
      </div>
    </article>
  )
}

export default BlogPost
